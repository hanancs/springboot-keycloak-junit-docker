package com.mongoDB.mongoDB.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongoDB.mongoDB.model.Student;
import com.mongoDB.mongoDB.service.StudentService;
import org.bson.types.ObjectId;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping(path = "/students",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Student> getAll(HttpServletResponse response) {
       return studentService.getAllStudents();
    }

    @GetMapping(path = "/students/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Student get(@PathVariable ObjectId id, HttpServletResponse response){
        for (Student obj :studentService.getAllStudents() ) {
            if (obj.getId().equals(id))
                return studentService.getStudent(id,response);
        }
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        return null;
    }

    @PostMapping(path="/students")
    @ResponseBody
    public String post(@RequestBody Student student,HttpServletResponse response) throws JsonProcessingException {
        return studentService.postStudent(student,response);
    }

    @DeleteMapping(path = "/students/{id}")
    public String delete(@PathVariable ObjectId id,HttpServletResponse response){
        return studentService.deleteStudent(id,response);
    }

    @PutMapping(path = "/students/{id}")
    public String update(@PathVariable("id") ObjectId id,
                                @RequestBody Student student, HttpServletResponse response) throws JsonProcessingException {
        return studentService.updateStudent(id, student, response);
    }

}
