package com.mongoDB.mongoDB.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongoDB.mongoDB.model.Teacher;
import com.mongoDB.mongoDB.service.TeacherService;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class TeacherController {

    private Logger logger = LoggerFactory.getLogger(TeacherController.class);
    private final TeacherService teacherService;

    @Autowired
    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping(path="/teachers", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Teacher> getAll(HttpServletResponse response){
        return teacherService.getAllTeachers();
    }

    @GetMapping(path = "/teachers/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Teacher get(@PathVariable ObjectId id, HttpServletResponse response){
        for (Teacher teacher:teacherService.getAllTeachers()){
            if(teacher.getId().equals(id)){
                return teacherService.getTeacher(id, response);
            }
        }
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        return null;
    }

    @PostMapping(path = "/teachers")
    @ResponseBody
    public String post(@RequestBody Teacher teacher, HttpServletResponse response) throws JsonProcessingException {
        return teacherService.postTeacher(teacher, response);
    }

    @DeleteMapping(path = "teachers/{id}")
    public String delete(@PathVariable ObjectId id, HttpServletResponse response){
        return teacherService.deleteTeacher(id, response);
    }

    @PutMapping(path = "teachers/{id}")
    public String update(@PathVariable ObjectId id, @RequestBody Teacher teacher, HttpServletResponse response) throws JsonProcessingException {
        return teacherService.updateTeacher(id, teacher, response);
    }

}

