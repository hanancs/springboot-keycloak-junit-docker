package com.mongoDB.mongoDB.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Transient;
import java.time.LocalDate;
import java.time.Period;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "student")
public class Student {

    private ObjectId id;
    private String name;
    private LocalDate dob;

    @Transient
    private int age;

    public int getAge() {
        return Period.between(this.dob,LocalDate.now()).getYears();
    }



}
