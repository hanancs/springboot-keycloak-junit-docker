package com.mongoDB.mongoDB.repository;

import com.mongoDB.mongoDB.model.Teacher;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TeacherRepository extends MongoRepository<Teacher, UUID> {

    Teacher deleteById(ObjectId id);

    boolean existsById(ObjectId id);

    Teacher findById(ObjectId id);
}
