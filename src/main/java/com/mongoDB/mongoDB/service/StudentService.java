package com.mongoDB.mongoDB.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongoDB.mongoDB.model.Student;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import com.mongoDB.mongoDB.repository.StudentRepository;


@Service
public class StudentService {

    private StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    public StudentService() {}

    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    public Student getStudent(ObjectId id, HttpServletResponse response) throws JSONException {
        boolean exists = studentRepository.existsById(id);
        if (!exists) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return studentRepository.findById(id);
    }

    public String postStudent(Student student, HttpServletResponse response) throws JsonProcessingException, JSONException {
        JSONObject responseJSON = new JSONObject();

        if (student.getName() == null) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            responseJSON.put("message", "User does not exist");
            return responseJSON.toString();
        }

        student = studentRepository.save(student);

        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        JSONObject studentJSON = new JSONObject(mapper.writeValueAsString(student));

        studentJSON.put("id", student.getId().toHexString());
        response.setStatus(HttpServletResponse.SC_CREATED);
        responseJSON.put("message", studentJSON);

        return responseJSON.toString();

    }

    public String deleteStudent(ObjectId id, HttpServletResponse response) throws JSONException {
        JSONObject responseJSON = new JSONObject();

        boolean exists = studentRepository.existsById(id);

        if (!exists) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            responseJSON.put("message", "User does not exist");
        } else {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            studentRepository.deleteById(id);
            responseJSON.put("deleted student with id ", id);
        }
        return responseJSON.toString();
    }

    @Transactional
    public String updateStudent(ObjectId id, Student student, HttpServletResponse response) throws JsonProcessingException, JSONException {
        JSONObject responseJSON = new JSONObject();
        boolean exists = studentRepository.existsById(id);
        Student studentObject = studentRepository.findById(id);

        if (!exists) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            responseJSON.put("message", "User does not exist");
            return responseJSON.toString();
        }

        if(student.getName()!=null) {
            studentObject.setName(student.getName());
            studentObject.setAge(student.getAge());
            studentRepository.save(studentObject);

            ObjectMapper mapper = new ObjectMapper();
            mapper.findAndRegisterModules();
            JSONObject studentJSON = new JSONObject(mapper.writeValueAsString(studentObject));

            studentJSON.put("id", studentObject.getId().toHexString());
            response.setStatus(HttpServletResponse.SC_CREATED);
            responseJSON.put("get updated", studentJSON);
        }

        return responseJSON.toString();

    }


}
