package com.mongoDB.mongoDB.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongoDB.mongoDB.model.Teacher;
import com.mongoDB.mongoDB.repository.TeacherRepository;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
public class TeacherService {

    private TeacherRepository teacherRepository;

    @Autowired
    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public TeacherService() {
    }

    public List<Teacher> getAllTeachers(){
        return teacherRepository.findAll();
    }

    public Teacher getTeacher(ObjectId id, HttpServletResponse httpServletResponse){
        boolean exists = teacherRepository.existsById(id);

        if(!exists) {
            httpServletResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

        return teacherRepository.findById(id);
    }

    public String postTeacher(Teacher teacher, HttpServletResponse httpServletResponse) throws JsonProcessingException, JSONException {

        JSONObject responseJSON = new JSONObject();
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();

        if( teacher.getName() == null){
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            responseJSON.put("message", "User does not exist");
            return responseJSON.toString();
        }

        teacher = teacherRepository.save(teacher);

        JSONObject teacherJSON = new JSONObject(mapper.writeValueAsString(teacher));
        teacherJSON.put("id", teacher.getId().toHexString());
        httpServletResponse.setStatus(HttpServletResponse.SC_CREATED);
        responseJSON.put("message", teacherJSON);

        return responseJSON.toString();

    }

    public String deleteTeacher(ObjectId id, HttpServletResponse response){
        JSONObject responseJSON = new JSONObject();
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();

        boolean exists = teacherRepository.existsById(id);

        if(!exists){
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            responseJSON.put("message", "User does not exist");
        } else{
            responseJSON.put("deleted teacher with id", id);
            teacherRepository.deleteById(id);
        }
        return responseJSON.toString();
    }

    @Transactional
    public String updateTeacher(ObjectId id, Teacher teacher, HttpServletResponse response) throws JsonProcessingException, JSONException {

        JSONObject responseJSON = new JSONObject();

        Teacher teacherObject = teacherRepository.findById(id);
        boolean exists = teacherRepository.existsById(id);

        if (!exists){
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            responseJSON.put("message", "User does not exist");
            return responseJSON.toString();
        }

        if(teacher.getName()!=null){
            teacherObject.setName(teacher.getName());
            teacherObject.setDob(teacher.getDob());
            teacherRepository.save(teacherObject);

            ObjectMapper mapper = new ObjectMapper();
            mapper.findAndRegisterModules();
            JSONObject teacherJSON = new JSONObject(mapper.writeValueAsString(teacherObject));

            teacherJSON.put("id", teacherObject.getId().toHexString());
            response.setStatus(HttpServletResponse.SC_CREATED);
            responseJSON.put("get updated with teacher id ", teacherJSON);
        }

        return responseJSON.toString();
    }




}

