package com.mongoDB.mongoDB;

import com.mongoDB.mongoDB.repository.StudentRepository;
import com.mongoDB.mongoDB.model.Student;
import com.mongoDB.mongoDB.service.StudentService;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTests {

    @Autowired
    private MockMvc mockMvc;
    public static final String URL_TEMPLATE = "/api";
    private static Student student;
    private static StudentService studentService;
    private static StudentRepository studentRepository;

    @BeforeEach
    public void setupTestSuit() throws JsonProcessingException {
        studentService = new StudentService();
        student = new Student();
    }

    @Test
    public void testCreateStudent() throws Exception {
        String newString =
                "{" +
                        "\"name\":\"arkam\"," +
                "\"dob\" : \"2000-05-20\" " +"}";

        this.mockMvc.perform(post(URL_TEMPLATE+"/students")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newString))
                .andExpect(status().isCreated());}

    @Test
    public void postBadRequest() throws Exception {
        String requestString = "\"username\":\"cassim\",\n" +
                "\"date\":\"1122234009221-23\",\n";

        this.mockMvc.perform(post(URL_TEMPLATE+"/students")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestString))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testReadStudent() throws Exception{
        mockMvc.perform(get(URL_TEMPLATE + "/students"))
                .andExpect(status().isOk());
    }

    @Test
    public void getStudentById() throws Exception {
        mockMvc.perform(get(URL_TEMPLATE + "/students/{id}", "61dabd7606129e19ec5f9abc"))
                .andExpect(status().isOk());
    }

    @Test
    public void getStudentByIdBadReq() throws Exception {
        mockMvc.perform(get(URL_TEMPLATE + "/students/{id}", "543"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateStudentSuccess() throws Exception {
        String requestString = "{" +
                "\"name\":\"janji\"," +
                "\"dob\" : \"1998-02-18\" " +"}";
        String updatedName = "janji";
        mockMvc.perform(put(URL_TEMPLATE + "/students/{id}", "61dac5c450b03a11211f4d14")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestString));
        mockMvc.perform(get(URL_TEMPLATE + "/students/{id}", "61dac5c450b03a11211f4d14"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is(updatedName)));
    }

    @Test
    public void deleteStudentById() throws Exception {
        mockMvc.perform(delete(URL_TEMPLATE + "/students/{id}", "61dabd7606129e19ec5f9abc"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteStudentNonExisting() throws Exception {
        mockMvc.perform(get(URL_TEMPLATE + "/students/{id}", "61c19a86a879c50d44dd967e"))
                .andExpect(jsonPath("$.errors").doesNotExist());
    }

}
